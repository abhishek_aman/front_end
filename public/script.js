var app = angular.module("autocomplete", []);

app.controller("controller", function($scope,$http) {
    
    // it have data array of objects
    // {
    //      id: 1,
    //      name: 'cement'
    // }
    $scope.searched_stocks_list = [];
	
    $scope.stockInfo = {};
    $scope.results = [];
    //var baseUrl = 'http://18.219.3.193:8080/';
    var baseUrl = 'http://localhost:8084/';
    var getstockUrl = 'Webservice/rest/Service/Stock?id=';
    var getSearchUrl = 'Webservice/rest/Service/GetSuggestion?text=';
    $scope.boolOnEnter=true;
    $scope.boolOnClick=true;
    // function which will hit api on keystroke down
    $scope.getAutoCompleteSuggestion = function(event){
        
        if(event.keyCode == 13){
            return true;
        }
        if($scope.textSearch.length==0){
           $scope.searched_stocks_list = [];
            return true;
           }
        // since it is binded into model
        // we have to search it in $scope.textSearch
        
        $http.get(baseUrl+getSearchUrl+$scope.textSearch).then(function(response){
            console.log(response);
            $scope.searched_stocks_list = response.data;
        });
        // hit get api into this
        // then set into an array 
    }
    
    
    $scope.getStockInfo = function(id){
        $scope.stockInfo = '';
        // http call to get the details 
        // of the selected stock
        $scope.boolOnClick=false;
        $scope.boolOnEnter=true;
        $http.get(baseUrl+getstockUrl+id).then(function(response){
            $scope.stockInfo = response.data;
            $scope.searched_stocks_list = [];
            $scope.textSearch = '';
            //console.log($scope.stockInfo);
        });
    }
    
    /*$http.get("http://localhost:8081/Webservice/rest/Service")
        .then(function(response) {
            $scope.stocks_list = response.data;
    });*/
    
    $scope.renderDisplayResult = function(event){
        console.log(event.keyCode);
        
        if(event.keyCode == 13){
            $scope.boolOnEnter=false;
            $scope.boolOnClick=true;

            $scope.results = $scope.searched_stocks_list;
            $scope.searched_stocks_list = [];
        }
    }
	
}); 
