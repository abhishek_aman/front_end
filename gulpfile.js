var gulp = require("gulp"),
    gulpWatch = require("gulp-watch"),
    browserSync = require("browser-sync");


gulp.task('default', function(){
    browserSync.init({
        server: './public/'     
    });
    
    gulpWatch('*/**/*.html', function(){
        browserSync.reload();
    })
});

